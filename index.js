const http = require("http")
const url = require("url")
const port = 4000;

const server = http.createServer((request, response) => {
    if(request.url == "/" && request.method == "GET"){
        response.writeHead(200, {"Content-Type" : "string/plain"});
        response.end("Welcome to booking system");
    }
    else if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to your profile!");
    }
    else if(request.url == "/courses" && request.method == "GET"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Here's our courses available!");
    }
    else if(request.url == "/addcourses" && request.method == "POST"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Add course to our resources");
    }
    else{
        response.end("Invalid Input!")
    }
})

server.listen(port);

console.log("Server is running at localhost:4000");